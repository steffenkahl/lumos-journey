using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace PTWO_PR
{
    /// <summary>
    /// A script for all objects that can be activated by the player
    /// </summary>
    public class Activator : MonoBehaviour
    {
        [SerializeField] private bool inRange; //if the activator is in range
        [SerializeField] private TMP_Text infoText; //the text that describes, what the player can do
        [SerializeField] private GameObject infoTextHolder; //The GO that holds the activator-text
        [SerializeField] private GameObject[] objectsToActivate; //Which objects should be activated if the activator is activated
        [SerializeField] private CollectedElementTypes neededElement; //What elements the activator needs to be available
        [Header("OnActivated Event")]
        [SerializeField] private UnityEvent onActivated; //A Unity Event for when the activator is activated
        private CharacterControllerMovement player; // A reference to the player
        private Camera mainCamera; //A reference to the main camera
        private bool isActivated; //If this activator was activated
        private ActiveSaveData activeSaveData; //A reference to the activeSaveData that holds the save file

        public bool IsActivated
        {
            get => isActivated;
            set => isActivated = value;
        }
        
        private void Start()
        {
            player = GameObject.FindGameObjectWithTag(GameTags.PLAYER).GetComponent<CharacterControllerMovement>(); //Get a reference to the player
            mainCamera = Camera.main; //Reference to the main Camera
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>(); //Get a reference to the ActiveSaveData
            HideInfo(); //Hide the activator-Info
        }

        private void Update()
        {
            if (mainCamera == null)
            {
                Debug.Log("Camera missing!");
            }
            infoTextHolder.transform.LookAt(mainCamera.transform.position); //Rotate the InfoText to the cameraPosition
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                if (!isActivated)
                {
                    ShowInfo(); //Show the Info for the activator
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                HideInfo(); //Hide the info for the activator
            }
        }

        /// <summary>
        /// Shows the activator-Info if all necessary elements were collected
        /// </summary>
        private void ShowInfo()
        {
            if (HasNecessaryElements())
            {
                //Only be activatable if the player has all necessary Elements
                inRange = true; 
                player.ActiveActivator = this;
                infoText.DOFade(1, 0.2f);
            }
        }
        
        /// <summary>
        /// Hides the activator-info
        /// </summary>
        private void HideInfo()
        {
            inRange = false;
            player.ActiveActivator = null;
            infoText.DOFade(0, 0.2f);
        }

        /// <summary>
        /// Activates the Activator (called from player) and checks if all needed things are correct
        /// </summary>
        public void ActivateActivator()
        {
            if (inRange)
            {
                if (HasNecessaryElements())
                {
                    Activate();
                }
            }
        }

        /// <summary>
        /// Activates the Activator and invokes the activator-event
        /// </summary>
        private void Activate()
        {
            isActivated = true;
            foreach (GameObject go in objectsToActivate)
            {
                go.SetActive(true);
            }
            onActivated.Invoke();
        }

        /// <summary>
        /// Deactivates the activator
        /// </summary>
        public void DeActivate()
        {
            isActivated = false;
            foreach (GameObject go in objectsToActivate)
            {
                go.SetActive(false); //Set all objects from the list active
            }
        }

        /// <summary>
        /// Checks if all necessary elements are collected by the player
        /// </summary>
        /// <returns>(bool) if all necessary elements are collected</returns>
        private bool HasNecessaryElements()
        {
            if (neededElement == CollectedElementTypes.NONE)
            {
                return true;
            }
            if (neededElement == CollectedElementTypes.EARTH && activeSaveData.GetData().hasEarthElement)
            {
                return true;
            }
            if (neededElement == CollectedElementTypes.WATER && activeSaveData.GetData().hasWaterElement)
            {
                return true;
            }
            if (neededElement == CollectedElementTypes.FIRE && activeSaveData.GetData().hasFireElement)
            {
                return true;
            }
            if (neededElement == CollectedElementTypes.WIND && activeSaveData.GetData().hasWindElement)
            {
                return true;
            }
            if (neededElement == CollectedElementTypes.ALL)
            {
                if (activeSaveData.GetData().hasEarthElement && activeSaveData.GetData().hasWaterElement && activeSaveData.GetData().hasFireElement &&
                    activeSaveData.GetData().hasWindElement)
                {
                    return true;
                }
            }
            return false;
        }
    }
}