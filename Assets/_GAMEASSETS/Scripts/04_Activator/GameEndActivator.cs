using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// This script handles the activation of the last element if all stones are filled with elements
    /// </summary>
    public class GameEndActivator : MonoBehaviour
    {
        [SerializeField] private GameObject avatarCollectible; //Reference to the AvatarObject that should get activated
        [SerializeField] private Activator[] activatorsToBeActive; //A list of Activators that need to be active in order to spawn the last element
        private bool wasActivated; //if this activatable Object was already activated

        public bool WasActivated
        {
            get => wasActivated;
            set => wasActivated = value;
        }
        
        private void Update()
        {
            if (!wasActivated)
            {
                bool allActive = true;
                foreach (Activator a in activatorsToBeActive)
                {
                    if (!a.IsActivated)
                    {
                        allActive = false; //If any needed Activator is not activated, not all are activated
                        break;
                    }
                }

                if (allActive)
                {
                    //If all needed Activators are active, activate the last element
                    avatarCollectible.SetActive(true);
                    wasActivated = true;
                }
            }
        }
    }
}