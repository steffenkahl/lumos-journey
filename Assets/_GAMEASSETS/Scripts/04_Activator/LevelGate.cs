using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// This script handles the level Gates to be openable when the player collected all Elements
    /// </summary>
    public class LevelGate : MonoBehaviour
    {
        [SerializeField] private GameObject doorObject; //Reference to the door
        [SerializeField] private bool doorIsOpen; //if the door should be open

        public bool DoorIsOpen
        {
            set => doorIsOpen = value; //Property to set the door open if needed
        }
        
        private void Update() 
        {
            if (doorIsOpen && doorObject.activeSelf)
            {
                doorObject.SetActive(false); //Disable the door if it should be open
            }
            else if (!doorIsOpen && !doorObject.activeSelf)
            {
                doorObject.SetActive(true); //Enable the door if it should be closed
            }
        }
    }
}