using UnityEngine;

namespace PTWO_PR
{
    //Script for the Clock at the end of the game that, when activated, activates the day night cycle and shows the current time as a grandfather clock
    public class WorldClockActivator : MonoBehaviour
    {
        [SerializeField] private GameObject minuteArrow; //A reference to the minute arrow of the clock
        [SerializeField] private GameObject hourArrow; //A reference to the hour arrow of the clock
        [SerializeField] private float degreesPerHour; //How many Degrees the hour arrow should rotate each hour
        [SerializeField] private float degreesPerMinute; //How many Degrees the minute arrow should rotate each minute
        private Animator animator; //A reference to the animator that enables swinging of the Pendulum
        private bool wasActivated; //A private variable to store if the Clock was already activated
        private DayNightCycleManager dayNightCycleManager; //A reference to the DayNighCycleManager
        private float timeOfDay; //A reference to the current time of day gotten from the DayNightCycleManager
        private ActiveSaveData activeSaveData;

        private void Start()
        {
            //Get references through GetComponent and Tags
            animator = GetComponent<Animator>();
            dayNightCycleManager = GameObject.FindGameObjectWithTag(GameTags.DAYNIGHTCYCLEMANAGER).GetComponent<DayNightCycleManager>();
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
            if(activeSaveData.GetData().doesDayNightCycle)
            {
                wasActivated = true;
            }
        }

        private void Update()
        {
            //Only change anything if the clock wasn't activated before
            if (wasActivated)
            {
                timeOfDay = dayNightCycleManager.timeOfDay; //Update the reference to the time of Day each frame
                hourArrow.transform.localRotation = Quaternion.Euler (0f, 0f, (int) timeOfDay * degreesPerHour); //Rotate the hour arrow
                minuteArrow.transform.localRotation = Quaternion.Euler (0f, 0f, (timeOfDay % 1) * 10 * degreesPerMinute); //Rotate the minute arrow
            }
        }

        public void Activate()
        {
            //Only allow activating if it wasn't activated before
            if (!wasActivated)
            {
                dayNightCycleManager.DoDayNightCycle(true);
                activeSaveData.DoesDayNightCycle(true);
                animator.SetBool("isActive", true);
                wasActivated = true;
            }
        }
    }
}
