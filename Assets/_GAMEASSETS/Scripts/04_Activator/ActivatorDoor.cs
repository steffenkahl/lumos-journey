using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// Script for an activatable door
    /// </summary>
    public class ActivatorDoor : MonoBehaviour
    {
        [SerializeField] private Activator[] activatorsToBeActive; //A list of activators that need to be activated
        private Animator animator; //A reference to the animator that opens the door
        private bool wasActivated; //If this activatable object was already activated

        private void Start()
        {
            //Find and set references
            animator = GetComponent<Animator>(); //Get the reference to the animator
        }

        private void Update()
        {
            if (!wasActivated)
            {
                bool allActive = true;
                foreach (Activator a in activatorsToBeActive)
                {
                    if (!a.IsActivated)
                    {
                        allActive = false; //if one is not active, not every needed one is active
                        break;
                    }
                }

                if (allActive)
                {
                    animator.SetBool("isOpen", true); //Activate the opening animation in the animator
                    wasActivated = true;
                }
            }
        }
    }
}