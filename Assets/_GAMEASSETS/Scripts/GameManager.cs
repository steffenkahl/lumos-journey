using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private string mainGameSceneName;
        private LevelGate[] levelGates;
        private ActiveSaveData activeSaveData;

        private void Awake()
        {
            if (GameObject.Find(gameObject.name) != this.gameObject) 
                //We have to make sure this is the only instance of this script to make sure no other script gets the wrong variables 
            {
                Destroy(this.gameObject);
            }

            DontDestroyOnLoad(this.gameObject); //Otherwise, this GO stays active throughout the whole game
        }

        private void Start()
        {
            GameObject[] levelGateGOs = GameObject.FindGameObjectsWithTag(GameTags.LEVELGATE);
            levelGates = new LevelGate[levelGateGOs.Length];
            for (int i = 0; i < levelGateGOs.Length; i++)
            {
                levelGates[i] = levelGateGOs[i].GetComponent<LevelGate>();
            }
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
        }

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnLevelFinishedLoading;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        }

        private void Update()
        {
            //As soon as all elements have been collected, all gates in the game open
            if (activeSaveData.GetData().hasEarthElement && activeSaveData.GetData().hasWaterElement && activeSaveData.GetData().hasFireElement &&
                activeSaveData.GetData().hasWindElement)
            {
                //Open all Gates
                foreach (LevelGate gate in levelGates)
                {
                    gate.DoorIsOpen = true;
                }
            }
        }

        void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
        {
            //Check for the currently loaded gameScene
            if (scene.name == mainGameSceneName)
            {
                //Hide All Objects tagged with "InvisibleInGame" to make them invisible in the game
                GameObject[] invisibleObjects = GameObject.FindGameObjectsWithTag(GameTags.INVISIBLEINGAME);
                foreach (GameObject invObject in invisibleObjects)
                {
                    //If the tagged GameObject has a renderer, disable it
                    if (invObject.GetComponent<MeshRenderer>())
                    {
                        invObject.GetComponent<MeshRenderer>().enabled = false;
                    }
                }
            }
        }
    }
}