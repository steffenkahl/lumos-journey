using UnityEngine;

namespace PTWO_PR
{ 
    public class Settings : MonoBehaviour
    {
        private float mouseSensitivity; //two variables for the values we want the player to be able to set themselves when playing the game

        private float volume;


        public float MouseSensitivity //Both private variables need properties so we can read and overwrite their values
        {
            get { return mouseSensitivity; }
            set { mouseSensitivity = value; }
        }

       public float Volume
        {
          get { return volume; }
          set { volume = value; }
        }

       private void Awake()
       {
           if (GameObject.FindGameObjectWithTag(GameTags.SETTINGS) != this.gameObject)
            {
                Destroy(this.gameObject);
            }//We have to make sure this is the only instance of this script to make sure no other script gets the wrong variables 

            DontDestroyOnLoad(this.gameObject);
       }

        /*We save the new values of the Volume and the Sensitivity in the PlayerPrefs when the Player changes them in the Settings 
         * (the Code used for this is "SetSlider")
         * When the Game is started, we get the saved Values from the PlayerPrefabs and assign them to our Variables*/
       private void Start()
       {
           volume = PlayerPrefs.GetFloat("SoundVolume", 0.75f); 
           mouseSensitivity = PlayerPrefs.GetFloat("MouseSensitivity", 0.65f); 
            //The MouseSensitivity is 0.65f as default, because 1.0f is too high, the player has the possibility to increase it (and decrease of course)
        }
    }
}
