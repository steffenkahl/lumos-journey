using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class MainMenuMusic : MonoBehaviour
    {
        [SerializeField] private bool doesPlayMusic;
        [SerializeField] private AudioSource audioSource; //Referencing
        [SerializeField] private string gameSceneName;
        private Settings settings; //Referencing

        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        private void Start()
        {
            //Finding References
            settings = GameObject.FindGameObjectWithTag(GameTags.SETTINGS).GetComponent<Settings>();
        }

        private void OnEnable() //When the scene is loaded 
        {
            SceneManager.sceneLoaded += OnSceneLoaded; 
        }

        private void OnDisable() //When the scene is unloaded
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode) 
            //if the name of the current scene is not our GameLevel (Meaning Prelaoder, MainMenu, Options, Credits or SaveData-Scene), this Music should play(doesPlayMusic = true)
        {
            if (scene.name == gameSceneName)
            {
                doesPlayMusic = false;
            }
            else
            {
                doesPlayMusic = true; 
            }
        }

        private void Update()
        {
            if (doesPlayMusic) //if Music should play(doesPlayMusic = true), get the volume of the music from our settings
            {
                audioSource.volume = settings.Volume;
            }
            else
            {
                audioSource.volume = 0f;
            }
        }
    }
}