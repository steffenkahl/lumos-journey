using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PTWO_PR
{
    //Logic for the main UI ingame
    public class MainGameUI : MonoBehaviour
    {
        //References and variables
        [SerializeField] private CharacterControllerMovement playerMover;
        [SerializeField] private GameObject saveInfoPanel;
        [SerializeField] private GameObject pauseGameMenuPanel;
        
        [SerializeField] private CollectableUI collectableUI;
        [SerializeField] private Image introPanel;
        [SerializeField] private float fadeInDuration;

        [Header("Outro")]
        [SerializeField] private GameObject outroPanel;
        [SerializeField] private TMP_Text outroCollectibleCount;
        [SerializeField] private float outroFadeDuration;
        [SerializeField] private string nameOfEndActivatorGO;
        private Activator[] activatorsToDeactivateOnGameEnd;
        
        private ActiveSaveData activeSaveData;
        private SaveSystemManager saveSystemManager;
        private Image saveInfoPanelImage;

        public CollectableUI CollectableUI => collectableUI;

        public Image IntroPanel
        {
            get => introPanel;
        }

        public GameObject SaveInfoPanel
        {
            get => saveInfoPanel;
            set => saveInfoPanel = value;
        }
        
        private void Start()
        {
            //Finding/Setting the references
            saveInfoPanelImage = saveInfoPanel.GetComponent<Image>();
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
            saveSystemManager = GameObject.FindGameObjectWithTag(GameTags.SAVESYSTEMMANAGER).GetComponent<SaveSystemManager>();
            playerMover = GameObject.FindGameObjectWithTag(GameTags.PLAYER).GetComponent<CharacterControllerMovement>();
            introPanel.DOFade(0, fadeInDuration);
            outroPanel.SetActive(false);
        }

        public void ShowSaveInfo()
        {
            saveInfoPanel.SetActive(true);
        }

        public void HideSaveInfo()
        {
            saveInfoPanel.SetActive(false);
        }

        public void HideSaveInfo(float delay)
        {
            saveInfoPanel.gameObject.GetComponent<Image>().DOFade(0, 0.2f).SetDelay(delay).OnComplete(HideSaveInfo);
        }

        public void ShowPauseGameMenu()
        {
            pauseGameMenuPanel.SetActive(true);
        }

        public void EndOfGame(Activator[] activatorsToDeactivateIn)
        {
            //Execute this code when you collect the avatar element as the last element
            ShowSaveInfo();
            saveSystemManager.SaveGame(saveSystemManager.CurrentSaveName);
            outroCollectibleCount.text = "You collected " + collectableUI.CollectedCollectables + " out of " + collectableUI.MAXCollectableCollectables +
                                         " Crystals";
            activatorsToDeactivateOnGameEnd = activatorsToDeactivateIn;
            introPanel.DOFade(1, outroFadeDuration).OnComplete(ShowOutroPanel);
        }

        private void ShowOutroPanel()
        {
            outroPanel.SetActive(true);
            playerMover.OnPause();
            //Cursor.lockState = CursorLockMode.None;
            
            //Deactivate the stones to make finishing again possible:
            foreach (Activator acti in activatorsToDeactivateOnGameEnd)
            {
                acti.DeActivate();
            }
            HideSaveInfo();
        }

        public void BTN_BackToMenu()
        {
            ShowSaveInfo(); //Show Saving Animation
            activeSaveData.SetPosition(playerMover.transform.position + Vector3.up * 0.1f);
            saveSystemManager.SaveAutoSave(saveSystemManager.CurrentSaveName);
            activeSaveData.SetData(new PlayerData());
            SceneManager.LoadScene("MainMenu");
        }

        public void BTN_ContinuePlaying()
        {
            outroPanel.SetActive(false);
            introPanel.DOFade(0, outroFadeDuration);
            GameObject.Find(nameOfEndActivatorGO).GetComponent<GameEndActivator>().WasActivated = false;
            //Cursor.lockState = CursorLockMode.Locked;
            playerMover.OnPause();
        }

        public void BTN_QuitGame()
        {
            ShowSaveInfo(); //Show Saving Animation
            activeSaveData.SetPosition(playerMover.transform.position + Vector3.up * 0.1f);
            saveSystemManager.SaveAutoSave(saveSystemManager.CurrentSaveName);
            Application.Quit();
            Debug.Log("Game quitting!");
        }
    }
}