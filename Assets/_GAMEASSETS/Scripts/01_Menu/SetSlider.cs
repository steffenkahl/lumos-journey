using UnityEngine;
using UnityEngine.UI;//require to access the Slider
using TMPro;

namespace PTWO_PR
{
    public class SetSlider : MonoBehaviour

    {
        [SerializeField]
        private Slider sensitivitySlider; //serialized to be able to drag and drop the Slider in the unity inspector

        [SerializeField]
        private Slider volumeSlider;

        private Settings settings;

        [SerializeField]
        private TextMeshProUGUI textMeshVolume; //serialized to be able to drag and drop the Text in the unity inspector

        [SerializeField]
        private TextMeshProUGUI textMeshSensitivity;



        private void Start()
        {
            settings = GameObject.FindGameObjectWithTag(GameTags.SETTINGS).GetComponent<Settings>(); 
            //Searching the GO with the Tag Settings, so we can accesss its variables
            sensitivitySlider.value = settings.MouseSensitivity;
            volumeSlider.value = settings.Volume;
        }

        public void SetMouseLevel()
        {
            settings.MouseSensitivity = sensitivitySlider.value;
            PlayerPrefs.SetFloat("MouseSensitivity", sensitivitySlider.value); //Saving the Set Value in the PlayerPrefs as soon as its changed by the player
        }

        public void SetVolumeLevel()
        {
            settings.Volume = volumeSlider.value;
            PlayerPrefs.SetFloat("SoundVolume", volumeSlider.value); //Saving the Set Value in the PlayerPrefs as soon as its changed by the player
        }

        public void ShowSliderValueVolume()
        {
            string sliderMessage = "Volume =" + (int)(volumeSlider.value * 100); //to show the float value in an int number
            textMeshVolume.text = sliderMessage; //to Display the Message in Text
        }

        public void ShowSliderValueSensitivity()
        {
            string sliderMessage = "Mouse Sensitivity =" + (int)(sensitivitySlider.value * 100); 
            textMeshSensitivity.text = sliderMessage;
        }

    }
}
