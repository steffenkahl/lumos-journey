using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// Script for the collectibles
    /// </summary>
    public class CollectableCrystal : MonoBehaviour
    {
        private ActiveSaveData activeSaveData;
        private CollectableUI collectableUI;
        private ParticleSystem collectibleParticleSystem;

        private void Awake()
        {
            collectibleParticleSystem = GetComponent<ParticleSystem>();
        }

        private void Start()
        {
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
            collectableUI = GameObject.FindGameObjectWithTag(GameTags.MAINGAMEUI).GetComponent<MainGameUI>().CollectableUI;
        }

        private void OnTriggerEnter(Collider other)
        {
            //Pickup Collectible if the player enters the trigger
            if (other.CompareTag(GameTags.PLAYER))
            {
                collectibleParticleSystem.Play(); //Show particles
                int id = int.Parse(gameObject.name); //Get the id of this collectible
                activeSaveData.AddCollectable(id); //Add the id of this collectible to the activesavedata as collected
                collectableUI.UpdateValues(); //Update the UI to display the correct value

                gameObject.transform.GetChild(0).gameObject.SetActive(false); //Disable the model of the collectible
                GetComponent<Collider>().enabled = false; //Disable the collider to not trigger again
            }
        }
    }
}