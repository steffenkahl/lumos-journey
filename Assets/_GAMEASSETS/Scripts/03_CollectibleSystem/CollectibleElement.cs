using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace PTWO_PR
{
    public class CollectibleElement : MonoBehaviour
    {
        [SerializeField] private CollectedElementTypes elementType;
        private ActiveSaveData activeSaveData;
        private CollectableUI collectableUI;

        private void Start()
        {
            //find and assign references
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
            collectableUI = GameObject.FindGameObjectWithTag(GameTags.MAINGAMEUI).GetComponent<MainGameUI>().CollectableUI;
        }

        private void OnTriggerEnter(Collider other) //if a collectible element has been picked up, it is destroyed and added to the save data
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                activeSaveData.AddElement(elementType);
                collectableUI.UpdateValues();

                Destroy(this.gameObject);
            }
        }

        public void CheckIfCollected() //if the elements have been collected, they dissapear in the world (can not be picked up multiple times)
        {
            switch (elementType)
            {
                case CollectedElementTypes.EARTH:
                    if (activeSaveData.GetData().hasEarthElement)
                    {
                        gameObject.SetActive(false);
                    }
                    break;
                case CollectedElementTypes.WATER:
                    if (activeSaveData.GetData().hasWaterElement)
                    {
                        gameObject.SetActive(false);
                    }
                    break;
                case CollectedElementTypes.FIRE:
                    if (activeSaveData.GetData().hasFireElement)
                    {
                        gameObject.SetActive(false);
                    }
                    break;
                case CollectedElementTypes.WIND:
                    if (activeSaveData.GetData().hasWindElement)
                    {
                        gameObject.SetActive(false);
                    }
                    break;
            }
            
        }
    }
}