using System.Collections;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    /// <summary>
    /// This script manages everything related to collectibles
    /// </summary>
    public class CollectibleManager : MonoBehaviour
    {
        [ReadOnly(true)][SerializeField] private int maxCollectables; //how many collectibles the player can have at max
        [SerializeField] private GameObject[] pickups; //A list of all collectibles
        [SerializeField] private bool[] collected; //List of bools what collectibles were collected
        [SerializeField] private string gameSceneName; //The name of the gameScene

        private ActiveSaveData activeSaveData; // A reference to the active save data
        private CollectableUI collectableUI; //The UI for displaying the collectible count

        public int MAXCollectables { get => maxCollectables; }
        
        private void Awake()
        {
            if (GameObject.FindGameObjectWithTag(GameTags.COLLECTIBLEMANAGER) != this.gameObject) 
                //have to make sure there is only one script of this to avoid bugs/wrong variables
            {
                Destroy(this.gameObject);
            }

            DontDestroyOnLoad(this.gameObject); //this stays active
        }
        
        private void Start()
        {
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
            collected = activeSaveData.GetData().collectibleCollected;
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (SceneManager.GetActiveScene().name == gameSceneName)
            {
                //Only load collectibles if the scene is the gameScene
                collectableUI = GameObject.FindGameObjectWithTag(GameTags.MAINGAMEUI).GetComponent<MainGameUI>().CollectableUI;
                StartCoroutine(GetCollectibles());
            }
            else
            {
                pickups = null;
                collected = null;
            }
        }

        /// <summary>
        /// Get all Collectibles after the scene is loaded with a small wait
        /// </summary>
        /// <returns></returns>
        IEnumerator GetCollectibles()
        {
            yield return new WaitForSeconds(0.1f); //Waits for 0.1 Seconds before loading collectibles so every collectible is already loaded
            pickups = GameObject.FindGameObjectsWithTag(GameTags.COLLECTIBLE); //Find all colelctibles as list
            collected = new bool[pickups.Length]; //set the length of the bool array
            maxCollectables = pickups.Length; //sets the max amount of collectibles to the number of collectables in game

            activeSaveData.InitializeCollectables(pickups.Length); //Initialize Collectibles in the save data
            collectableUI.UpdateValues(); //Update the displayed values in UI

            int i = 0;
            foreach (GameObject go in pickups)
            {
                go.SetActive(true);
                go.name = i.ToString(); //Change the name of the collectable to make counting easier
                collected[i] = false;
                i++;
            }

            for (int j = 0; j < collected.Length; j++)
            {
                if (activeSaveData.GetData().collectibleCollected[j])
                {
                    pickups[j].SetActive(false); //if the collectable is already collected, disable it
                }
            }

            foreach (GameObject go in GameObject.FindGameObjectsWithTag(GameTags.COLLECTIBLEELEMENT))
            {
                go.GetComponent<CollectibleElement>().CheckIfCollected();
            }
        }
    }
}