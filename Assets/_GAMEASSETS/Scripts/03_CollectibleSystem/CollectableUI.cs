using TMPro;
using UnityEngine;

namespace PTWO_PR
{
    //Script to handle the UI for the collectables in the main UI
    public class CollectableUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text collectableCount;
        [SerializeField] private GameObject earthElementIcon;
        [SerializeField] private GameObject waterElementIcon;
        [SerializeField] private GameObject fireElementIcon;
        [SerializeField] private GameObject windElementIcon;
        private CollectibleManager collectibleManager;

        private ActiveSaveData activeSaveData; //Reference to the currently loaded saveData
        private int collectedCollectables; //private variable for the amount of collected Collectables
        private int maxCollectableCollectables; //private variable for the maximum amount of collectibles you can obtain
        private int collectedElements; //private variable for the amount of collected Collectables

        public bool hasEarthElement;
        public bool hasWaterElement;
        public bool hasFireElement;
        public bool hasWindElement;

        public int CollectedCollectables => collectedCollectables;

        public int MAXCollectableCollectables => maxCollectableCollectables;
        
        private void Start()
        {
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
            collectibleManager = GameObject.FindGameObjectWithTag(GameTags.COLLECTIBLEMANAGER).GetComponent<CollectibleManager>();
            collectedCollectables = activeSaveData.GetData().collectedCollectables;
            UpdateValues(); //Update the values to show the correct numbers when starting the game
        }

        public void UpdateValues()
        {
            collectedCollectables = activeSaveData.GetData().collectedCollectables;
            hasEarthElement = activeSaveData.GetData().hasEarthElement;
            hasWaterElement = activeSaveData.GetData().hasWaterElement;
            hasFireElement = activeSaveData.GetData().hasFireElement;
            hasWindElement = activeSaveData.GetData().hasWindElement;
            SetVisibilities(); //After loading the current numbers, show them!
        }

        private void SetVisibilities()
        {
            collectableCount.text = collectedCollectables.ToString() + " / " + collectibleManager.MAXCollectables;
            maxCollectableCollectables = collectibleManager.MAXCollectables;
            
            earthElementIcon.SetActive(false);
            waterElementIcon.SetActive(false);
            fireElementIcon.SetActive(false);
            windElementIcon.SetActive(false);
            
            //Display the collected Elements:
            if (hasEarthElement)
            {
                earthElementIcon.SetActive(true);
            }
            if (hasWaterElement)
            {
                waterElementIcon.SetActive(true);
            }
            if (hasFireElement)
            {
                fireElementIcon.SetActive(true);
            }
            if (hasWindElement)
            {
                windElementIcon.SetActive(true);
            }
        }
    }
}