using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    //The main Player Controller Script that handles movement and includes everything another player related script could need
    public class CharacterControllerMovement : MonoBehaviour
    {
        #region Serialized Variables---

        // Serializing values to assign them in the Unity Inspector
        [Header("Speeds")] [Tooltip("The speed that the character uses when walking casually")] [SerializeField]
        private float walkSpeed; //the medium speed, the character uses when walking casually

        [Tooltip("The speed that the character uses when running")] [SerializeField]
        private float runSpeed; //the fastest speed, the character uses when running

        [Tooltip("The speed that the character uses when gliding")]
        [SerializeField]
        private float glidingSpeed; //the speed, the character uses when gliding (similar to flying)

        [Tooltip("The inital runSpeed")]
        [SerializeField]
        private float initialRunSpeed; //the speed, the character uses at the beginning. Used to be able to change the speed back when changing runSpeed for glidingSpeed

        [SerializeField] private float modelRotateSpeed; //How fast the character rotates to the correct rotation when moving

        [SerializeField] private Transform firstSpawn;

        //PHYSICS
        [Header("Gliding and Jumping")] 
        [Tooltip("The normal gravity of the player")] [SerializeField] private float gravity; //use the gravity of the earth: -9.81

        [Tooltip("The gravity of the player when gliding")] [SerializeField]
        private float glideGravity;

        [Tooltip("How long the gravity changes to gliding gravity")] [SerializeField]
        private float glidingTime;

        [Tooltip("How high the player can jump")] [SerializeField]
        private float jumpForce; //The force which should be applied upwards when jumping

        [Tooltip("How fast the jump gets slower")] [SerializeField]
        private float jumpSmoother; //How smooth the jump speed should decrease

        [Tooltip("How high the player can jump on a HighJumpingPlatform")] [SerializeField]
        private float highJumpForce; //Height of jump on a High Jump Platform

        [Tooltip("If the player can HighJump")] [SerializeField]
        private bool canHighJump; //If the Player is standing on a High Jump Platform

        //REFERENCES
        [Header("References")] 
        [Tooltip("The Input System for the Player")] [SerializeField] private PlayerInput playerInput;
        [SerializeField] private LumosLight lumosLight;
        [SerializeField] private Transform playerModelTransform;
        private Animator animator;
        private ActiveSaveData activeSaveData;
        private Camera mainCamera;

        [Tooltip("A reference to the pause menu canvas")] [SerializeField]
        private GameObject pauseMenu; //The Pause Menu that should be seen after pausing the game

        #endregion

        #region Other Variables---

        private float moveSpeed; //the speed the character is currently moving with aka initial speed

        private float currentGravity; //the current gravity for the player

        private CharacterController characterController; //Storing the CharacterController Component
        private Vector3 motion; //Vector3 we use to move the CharacterController
        private Vector3 jump; //Vector 3 which describes the jump vector

        private Vector3 playerVelocity; //the current velocity of the player
        private Vector3 lastPosition; //the position the player had last frame
        private Tween gravityTween; //The tween for changing the gravity when gliding

        private Vector2 rawInputAxis; //the input Movement from the Input System (declared in "OnMovement()")
        private bool isSprinting; //if the player is sprinting
        private bool isGliding; //if the player is gliding
        private bool isPaused = true; //if the player is paused

        private MainSavePoint activeSavePoint;
        private Activator activeActivator;

        private Vector3 floorNormal;
        private bool isGroundedOnFlatGround;
        private float slopeLimit;

        private bool animateJump;

        #endregion


        //Properties for private variables we need to access from different scripts
        public bool CanHighJump
        {
            get { return canHighJump; }
            set { canHighJump = value; }
        }

        public bool IsPaused
        {
            get => isPaused;
            set => isPaused = value;
        }

        public MainSavePoint ActiveSavePoint
        {
            get => activeSavePoint;
            set => activeSavePoint = value;
        }

        public Activator ActiveActivator
        {
            get => activeActivator;
            set => activeActivator = value;
        }

        /// <summary>
        /// Before we can go through with Update, we need to attach the script to a GameObject with a 
        /// CharacterController-Component to be able to even access a CharacterController and store it in a variable
        /// </summary>
        private void Awake()
        {
            initialRunSpeed = runSpeed; //Saving the RunSpeed in another Variable
            
            characterController = GetComponent<CharacterController>();
            animator = GetComponent<Animator>();
            slopeLimit = characterController.slopeLimit;
            isGroundedOnFlatGround = characterController.isGrounded;
        }

        private void Start()
        {
            OnPause(); //Deactivate the Pause 
            currentGravity = gravity; //Set the current gravity to the given basic gravity
            mainCamera = Camera.main; //Get a reference to the main Camera
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>(); //Get the reference to the active Save Data
            
            //Set the position of the player to the saved position:
            characterController.enabled = false;
            if (activeSaveData.GetPosition() == new Vector3(0, 0, 0))
            {
                transform.position = firstSpawn.position;
            }
            else
            {
                transform.position = activeSaveData.GetPosition();
            }
            characterController.enabled = true;
        }

        //Everything Physics Based should be in FixedUpdate to be consistent (refer ProjectDoucmentation "Adding HighJump")
        private void FixedUpdate()
        {
            if (!isPaused)
            {
                //Get Input Axis:
                float horizontalInput = rawInputAxis.x;
                float verticalInput = rawInputAxis.y;
                
                if (jump.y > 0) //If the jumpVector is not 0 it should decrease by the jumpSmooth-value
                {
                    jump.y -= jumpSmoother;
                }
                else
                {
                    jump.y = 0f;
                }
                
                //Calls OnJumpHold() when the jump button gets pressed for a longer time)
                if (playerInput.actions["jump"].phase == InputActionPhase.Performed)
                {
                    OnJumpHold();
                }

                //Calculate Player Velocity:
                Vector3 moved = transform.position - lastPosition; //calculate displacement since last Update
                lastPosition = transform.position; // update lastPos
                playerVelocity = moved / Time.deltaTime; // calculate the velocity

                //Calculate Move Speed:
                if (isSprinting && isGliding == false) //Running with Shift, but if you are Gliding you cannot Glide faster with Shift
                {
                    moveSpeed = runSpeed;
                }
                else
                {
                    moveSpeed = walkSpeed;
                }

                //Here we get the direction of the camera to make the movement relative to that
                Vector3 cameraForward = mainCamera.transform.forward;
                Vector3 cameraRight = mainCamera.transform.right;
                cameraForward.y = 0f;
                cameraRight.y = 0f;
                cameraForward.Normalize();
                cameraRight.Normalize();

                // We need to get the input values of the axis every frame in order tu move in the direction of our input
                // x is the horizontalInput value which will move the characterController right or left
                // y is set to the gravity value which will pull the characterController down 
                // z is set to the verticalInput value which will move the characterController forwards or backwards
                //motion = new Vector3(horizontalInput, gravity, verticalInput);
                Vector3 desiredMoveDirection = (cameraForward * verticalInput + cameraRight * horizontalInput) * moveSpeed;

                motion = desiredMoveDirection - transform.up * currentGravity + jump;
                
                //Sliding Mechanism:
                if (isGroundedOnFlatGround)
                {
                    motion.x += (1f - floorNormal.y) * floorNormal.x * (glidingSpeed * 1.2f);
                    motion.z += (1f - floorNormal.y) * floorNormal.z * (glidingSpeed * 1.2f);
                }

                if ((Vector3.Angle(Vector3.up, floorNormal) <= slopeLimit))
                {
                    isGroundedOnFlatGround = false;
                }
                else
                {
                    isGroundedOnFlatGround = characterController.isGrounded;
                }
                

                // We apply the Vector for movement (named "motion") to the .Move() method of the characterController 
                // In order to stay framerate independent we multiply the vector by Time.deltaTime and also
                // by the moveSpeed in order to maintain control of speed
                characterController.Move(motion: motion * Time.deltaTime);

                if (desiredMoveDirection != Vector3.zero)
                {
                    playerModelTransform.transform.rotation = Quaternion.Lerp(playerModelTransform.transform.rotation, 
                        Quaternion.LookRotation(desiredMoveDirection), Time.deltaTime * modelRotateSpeed);
                }
            }

            //ANIMATIONS ===
            if (motion.x > 0.5f || motion.z > 0.5f || motion.x < -0.5f || motion.z < -0.5f)
            {
                animator.SetBool("isWalking", true);
                if (moveSpeed > walkSpeed)
                {
                    animator.SetBool("isSprinting", true);
                }
                else
                {
                    animator.SetBool("isSprinting", false);
                }
            }
            else
            {
                animator.SetBool("isWalking", false);
            }

            if (animateJump)
            {
                animator.SetBool("isJumping", true);
                animateJump = false;
            }
            else
            {
                animator.SetBool("isJumping", false);
            }
            
            animator.SetBool("isGliding", isGliding);
            //===

            //reset falling and gliding if player touches ground
            if (characterController.isGrounded)
            {
                isGliding = false;
            }

            //Check if player glides:
            if (isGliding)
            {
                if (gravityTween == null)
                {
                    gravityTween = DOTween.To(() => currentGravity, x => currentGravity = x, glideGravity, glidingTime);
                }
                else if (!gravityTween.IsPlaying())
                {
                    gravityTween = DOTween.To(() => currentGravity, x => currentGravity = x, glideGravity, glidingTime);
                }

                //currentGravity = glideGravity;
            }
            else
            {
                //Reset gravity and kill the gravity Tween if the player no longer glides
                if (gravityTween != null)
                {
                    gravityTween.Kill();
                    gravityTween = null;
                }

                currentGravity = gravity;
            }

            if (isGliding) //checking if the Player is glidiing 
            {
             
                runSpeed = glidingSpeed; //if we are gliding, change the speed to the gliding speed to become slower
            }
            else if (isGliding == false)
            {
                runSpeed = initialRunSpeed; //runSpeed is the initalRunSpeed that we saved in Awake 
            }
        }


        //The input Actions that get called from the new input manager
        #region InputActions

        //Gets called when the Player uses Movement-Keys/Buttons
        public void OnMovement(InputAction.CallbackContext value)
        {
            rawInputAxis = value.ReadValue<Vector2>();
        }

        //Gets called when the Player uses the Jump-Key/Button
        public void OnJump(InputAction.CallbackContext value)
        {
            if (!isPaused)
            {
                if (value.started)
                {
                    animateJump = true;
                    
                    if (isGroundedOnFlatGround && canHighJump == false)
                    {
                        jump = new Vector3(0f, jumpForce, 0f);
                    }
                    else if (isGroundedOnFlatGround && canHighJump)
                    {
                        if (activeSaveData.GetData().hasFireElement)
                        {
                            jump = new Vector3(0f, highJumpForce, 0f);
                        }
                    }
                }
                if (value.canceled)
                {
                    isGliding = false;
                }
            }
        }

        //Gets called when the Player holds the Jump-Key/Button
        public void OnJumpHold()
        {
            if (playerVelocity.y <= 0)
            {
                isGliding = true;
            }
        }

        //Gets called when the Player uses the Sprint-Key/Button
        public void OnSprint(InputAction.CallbackContext value)
        {
            if (!isPaused)
            {
                if (value.performed)
                {
                    isSprinting = true;
                }
                else
                {
                    isSprinting = false;
                }
            }
        }


        public void OnInteract(InputAction.CallbackContext value)
        {
            if (activeSavePoint != null)
            {
                activeSavePoint.SaveTheGame();
            } else if (activeActivator != null)
            {
                activeActivator.ActivateActivator();
            }
            else
            {
                //TODO: Do World Interaction
            }
        }

        public void OnHeadlightSwitch(InputAction.CallbackContext value)
        {
            //Only allow Light Switching if Earth-Element has been collected
            if (activeSaveData.GetData().hasEarthElement)
            {
                lumosLight.Switch();
            }
        }

        /// <summary>
        /// Locks and unlocks the cursor when the button for "Pause Game" is pressed and shows the Pause Menu
        /// </summary>
        /// <param name="value">Value from Button-Input for the Input System</param>
        public void OnPause(InputAction.CallbackContext value)
        {
            isPaused = !isPaused;

            if (isPaused)
            {
                Cursor.lockState = CursorLockMode.None;
                pauseMenu.SetActive(true);
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                pauseMenu.SetActive(false);

            }
        }

        #endregion

        public void OnPause()
        {
            isPaused = !isPaused;

            if (isPaused)
            {
                Cursor.lockState = CursorLockMode.None;
                pauseMenu.SetActive(true);
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                pauseMenu.SetActive(false);
            }
        }

        public void OnBackToMenu()
        {
            activeSaveData.SetData(new PlayerData());
            SceneManager.LoadScene("MainMenu");
            //TODO: Put this into a pause menu manager please!!!
        }

        public void OnGameQuit()
        {
            Application.Quit();
            Debug.Log("Game quitting!");
        }
        
        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            //Checking for Slopes to slide off
            float radian = characterController.slopeLimit * Mathf.Deg2Rad;

            if (hit.normal.y <= radian)
            {
                floorNormal = hit.normal;
            } 
            else
            {
                floorNormal = Vector3.one;
            }
        }

        public void OnQuickSave(InputAction.CallbackContext value)
        {
            StartCoroutine(ShowSavingInfo(2));  
            SaveSystemManager saveManager = GameObject.FindGameObjectWithTag(GameTags.SAVESYSTEMMANAGER).GetComponent<SaveSystemManager>();
            saveManager.SaveQuickSave(saveManager.CurrentSaveName);
        }
        
        IEnumerator ShowSavingInfo (float delay) {
            MainGameUI mainUI = GameObject.FindGameObjectWithTag(GameTags.MAINGAMEUI).GetComponent<MainGameUI>();
            mainUI.ShowSaveInfo();
            yield return new WaitForSeconds(delay);
            mainUI.HideSaveInfo();
        }
    }
}
