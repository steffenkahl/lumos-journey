using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PTWO_PR
{
    //Controller for the main Camera of the Player
    [RequireComponent(typeof(CinemachineFreeLook))]
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float sensitivityModifierX; //A Modifier for the sensitivity in X Axis
        [SerializeField] private float sensitivityModifierY; //A Modifier for the sensitivity in Y Axis
        private float mouseSensitivity; //a private variable for the mouseSensitivity read from the settings
        private Vector2 rawLookingAxis; //The raw input for movement from the new input system

        private CharacterControllerMovement characterControllerMovement; //Reference to the player movement Controller
        private Settings settings; //Reference to the settings for the mouseSensitivity
        private CinemachineFreeLook freeLookCam; //Reference to the main Cinemachine

        private void Start()
        {
            //Get the references to work with
            settings = GameObject.FindGameObjectWithTag(GameTags.SETTINGS).GetComponent<Settings>();
            characterControllerMovement = GameObject.FindGameObjectWithTag(GameTags.PLAYER).GetComponent<CharacterControllerMovement>();
            freeLookCam = GetComponent<CinemachineFreeLook>();
            
            //Set the private variable to the value of the settings
            mouseSensitivity = settings.MouseSensitivity;
        }

        private void LateUpdate()
        {
            float mouseX = (rawLookingAxis.x * mouseSensitivity) * sensitivityModifierX;
            float mouseY = -(rawLookingAxis.y * mouseSensitivity) * sensitivityModifierY;

            //Set the input values for the cinemachine to the values from the inputsystem * sensitivity
            freeLookCam.m_XAxis.m_InputAxisValue = mouseX;
            freeLookCam.m_YAxis.m_InputAxisValue = mouseY;
        }

        public void OnLooking(InputAction.CallbackContext value)
        {
            if (!characterControllerMovement.IsPaused)
            {
                //Get the raw Axis Values for Looking
                rawLookingAxis = value.ReadValue<Vector2>(); //Reads the Values of the mouse or right joystick
            }
            else
            {
                rawLookingAxis = Vector2.zero;
            }
        }

        public void UpdateSensitivity()
        {
            mouseSensitivity = settings.MouseSensitivity;
        }
    }
}