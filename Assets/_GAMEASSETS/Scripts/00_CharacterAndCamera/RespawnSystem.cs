using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    /// <summary>
    /// Script for the complete respawning system. If a player triggers a DeathTrigger Respawn() will get called.
    /// </summary>
    public class RespawnSystem : MonoBehaviour
    {
        [SerializeField] private float fadeDuration; //for fading out the screen

        //References
        private MainGameUI mainGameUI; //Reference to the mainGameUI that holds the introPanel
        private CharacterController characterController; //reference to the character controller from the player
        private Image introPanel; //Reference to the introPanel for fading in and out

        private void Start()
        {
            //Get needed References
            characterController = GetComponent<CharacterController>();
            mainGameUI = GameObject.FindGameObjectWithTag(GameTags.MAINGAMEUI).GetComponent<MainGameUI>();
            introPanel = mainGameUI.IntroPanel;
        }

        /// <summary>
        /// Start the respawning with fade and position change
        /// </summary>
        public void Respawn()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(introPanel.DOFade(1, fadeDuration).OnComplete(ChangePlayerPosition));
            sequence.Append(introPanel.DOFade(0, fadeDuration));
        }

        /// <summary>
        /// Change the player position to the nearest Respawnpoint
        /// </summary>
        public void ChangePlayerPosition()
        {
            Vector3 targetRespawn = GetClosestRespawn(GetAllSpawnPoints());
            characterController.enabled = false;
            transform.position = targetRespawn;
            characterController.enabled = true;
        }

        /// <summary>
        /// Get all Spawnpoints as a list of positions
        /// </summary>
        /// <returns>(Vector3[]) Spawnpoints</returns>
        private Vector3[] GetAllSpawnPoints()
        {
            GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag(GameTags.SPAWNPOINT);
            Vector3[] spawnTransforms = new Vector3[spawnPoints.Length];

            for (int i = 0; i < spawnPoints.Length; i++)
            {
                spawnTransforms[i] = spawnPoints[i].transform.position;
            }
            return spawnTransforms;
        }
        
        /// <summary>
        /// Get the closest spawn point from a list of positions
        /// </summary>
        /// <param name="spawnPoints">(Vector3[]) List of positions</param>
        /// <returns>(Vector3) the nearest position to the player</returns>
        private Vector3 GetClosestRespawn(Vector3[] spawnPoints)
        {
             Vector3 targetMin = Vector3.zero;
             float minDistance = Mathf.Infinity; //Set the minimumdistance to infinity, so every spawnpoint is nearer than no spawnpoint
             Vector3 currentPos = transform.position;
             
             foreach (Vector3 targetTransform in spawnPoints)
             {
                 //Check each transform in the list of spawnpoints if it is nearer than the last one
                 float dist = Vector3.Distance(targetTransform, currentPos);
                 if (dist < minDistance)
                 {
                     targetMin = targetTransform;
                     minDistance = dist;
                 }
             }
             return targetMin;
        }
    }
}