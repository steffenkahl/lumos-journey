using UnityEngine;

namespace PTWO_PR
{
    public class MusicController : MonoBehaviour
    {
        [SerializeField] private bool audioSource1isPlaying; //checking if our first audiosource is playing or if we can switch it
        [SerializeField] private AudioSource audioSource1;
        [SerializeField] private AudioSource audioSource2;
        [SerializeField] private float fadeSpeed; //duration fo the fade (between the two audiosources

        private Settings settings; //a reference to the settings in which we store the volume

        private AudioSource ActiveAudioSource => audioSource1isPlaying ? audioSource1 : audioSource2;
        private AudioSource FadingAudioSource => audioSource1isPlaying ? audioSource2 : audioSource1;
        private AudioClip targetAudioClip;

        private void Start()
        {
            //Get a reference to the settings
            settings = GameObject.FindGameObjectWithTag(GameTags.SETTINGS).GetComponent<Settings>();
            audioSource1.volume = settings.Volume; //and set our audiosource volume to the volume the player set in the options
        }

        public void ChangeSong(AudioClip newClip)
        {
            targetAudioClip = newClip;
        }

        private void Swap()
        {
            audioSource1isPlaying = !audioSource1isPlaying;
            ActiveAudioSource.clip = targetAudioClip;
            ActiveAudioSource.Play();
        }

        private void Lerp(AudioSource source, float targetVolume, float speed) 
            /*Lerp = the interpolation value between two floats. 
             * Alias a matehamtical funtion that returns a value between two others at a point on a linear scale
             * This is for fading*/
        {
            float diff = source.volume - targetVolume;

            if (diff == 0)
            {
                return;
            }

            float portion = Mathf.Abs(speed * Time.deltaTime / diff);

            source.volume = Mathf.Lerp(source.volume, targetVolume, portion);
        }

        void Update()
        {
            float targetVolume;

            if (ActiveAudioSource.clip)
            {
                targetVolume = settings.Volume;
            }
            else
            {
                targetVolume = 0;
            }

            Lerp(ActiveAudioSource, targetVolume, fadeSpeed);
            Lerp(FadingAudioSource, 0, fadeSpeed);

            if (targetAudioClip && targetAudioClip != ActiveAudioSource.clip)
            {
                // Swap right away if target song just played and haven't faded out yet
                if (targetAudioClip == FadingAudioSource.clip)
                {
                    Swap();
                }
                // Swap once fading source is quiet
                else if (FadingAudioSource.volume < 0.01f)
                    Swap();
            }
        }
    }
}