using UnityEngine;

namespace PTWO_PR
{
    public class HighJumpPlatform : MonoBehaviour
    {
       
        private CharacterControllerMovement character;

        private void Start()
        {
            character = GameObject.FindGameObjectWithTag(GameTags.PLAYER).GetComponent<CharacterControllerMovement>();
        }

        /// <summary>
        /// OnTriggerEnter is called  when:
        /// 1) We have any kind of Collider attached
        /// 2) The other GameObject we collide with has any kind of Collider attached
        /// 3) The other Gameobject has also a Rigidbody Component
        /// 4) The collider of the other GameObject is marked as "Is Trigger"
        /// This method will be called only once upon first entering the other collider
        /// </summary>
        private void OnTriggerEnter(Collider player)
        {
            // With the .CompareTag method we can compare tags more efficiently than with ==
            // We check for the tag since we only want to be damaged by GameObejcts which are tagged as an Enemy
            if (player.gameObject.CompareTag(GameTags.PLAYER))
            {
                character.CanHighJump = true;
                Debug.Log(message: "HighJump should be possible");
            }
            Debug.Log(message: "Tag of the GO " + player.gameObject.tag);
            Debug.Log(message: "The Player has Collided with the Platform");
        }

        private void OnTriggerExit(Collider player) //If the player leaves the high Jump plattform the bool should be wrong, so that he can not high jump afterwards
        {
            if (player.gameObject.CompareTag(GameTags.PLAYER))
            {
                character.CanHighJump = false;
            }
            Debug.Log(message: "The Player has left the Platform");
        }


    }
}