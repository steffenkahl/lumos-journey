using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// Script to change Music when entering the trigger
    /// </summary>
    public class MusicTrigger : MonoBehaviour
    {
        [SerializeField] private AudioClip musicToPlay;
        private MusicController musicController;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER)) //if the player collides with our invisible walls (this trigger) the Song will change
            {
                musicController = other.GetComponent<MusicController>();

                musicController.ChangeSong(musicToPlay);
            }
        }
    }
}