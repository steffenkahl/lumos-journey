using UnityEngine;

namespace PTWO_PR
{
    public class DeathTrigger : MonoBehaviour
    {
        private RespawnSystem playerRespawn; //Referencing

        private void Start()
        {
            //Finding and setting the Reference
            playerRespawn = GameObject.FindGameObjectWithTag(GameTags.PLAYER).GetComponent<RespawnSystem>();
        }

        private void OnTriggerEnter(Collider other)
        {
            playerRespawn.Respawn(); //Make the player respawn if the player collides with the death trigger
        }
    }
}
