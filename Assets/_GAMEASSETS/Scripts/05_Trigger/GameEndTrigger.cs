using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    /// <summary>
    /// This script handles the moment when the player collects the last element in the game
    /// </summary>
    public class GameEndTrigger : MonoBehaviour
    {
        [SerializeField] private Activator[] activatorsToDeactivate;
        private MainGameUI mainGameUI;
        private Image introPanel;

        private void Start()
        {
            //Finding and setting references
            mainGameUI = GameObject.FindGameObjectWithTag(GameTags.MAINGAMEUI).GetComponent<MainGameUI>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                mainGameUI.EndOfGame(activatorsToDeactivate); //Activate the EndOfGame()-Method in the mainGameUI
                gameObject.SetActive(false); //disable this object to not be collectible multiple times in the same time
            }
        }
    }
}