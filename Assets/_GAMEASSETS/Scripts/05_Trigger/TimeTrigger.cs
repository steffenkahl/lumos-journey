using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// This script triggers time changes when entering a trigger
    /// </summary>
    public class TimeTrigger : MonoBehaviour
    {
        [SerializeField] private float targetTime; //The time to switch to after entering
        [SerializeField] private float targetSpeed; //How fast the time should change
        private DayNightCycleManager dayNightCycleManager; //A reference to the daynightcyclemanager

       
        void Start()
        {
            //Finding and setting references
            dayNightCycleManager = GameObject.FindGameObjectWithTag(GameTags.DAYNIGHTCYCLEMANAGER).GetComponent<DayNightCycleManager>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER)) 
                //if the player collides with the invisible triggerwalls (Which will have this script assigned) the time will change accordingly
            {
                Debug.Log("Triggered Time Change!");
                if (!dayNightCycleManager.DayNightCycleIsActive())
                {
                    dayNightCycleManager.ChangeDayTime(targetTime, targetSpeed);
                }
            }
        }
    }
}