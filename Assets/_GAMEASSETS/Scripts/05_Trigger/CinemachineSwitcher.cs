using UnityEngine;
using Cinemachine;

namespace PTWO_PR
{
    public class CinemachineSwitcher : MonoBehaviour
    {
        [SerializeField]
        private CinemachineFreeLook vCamOutside; //Assign the outside camera here

        [SerializeField]
        private CinemachineFreeLook vCamIndoors; // Assign the inside camera here

        [SerializeField]
        private bool isInsidetheHouse; 
        //since we have two colliders, which trigger  what camer is going to have the highest priority, we need to check which one leads outside the house 
        //(alias is currently inside)

        //The priorites of the cameras
        [SerializeField]
        private int highPriority;
        [SerializeField]
        private int lowPriority;

        public void OnTriggerEnter(Collider other) 
        {
            if (other.CompareTag(GameTags.PLAYER)) //if a Go with the GameTag Player enters the trigger's collider...
            {
                if (isInsidetheHouse) //and the collider is inside the house(which means it leads outside)
                {
                    if (vCamOutside.Priority != highPriority)
                    {
                        vCamOutside.Priority = highPriority;
                        vCamIndoors.Priority = lowPriority;
                        //the Outside camera should have the highest priority so the palyer looks through it
                    }
                }
                else //otherwise the inside camera has the hightest prio
                {
                    vCamIndoors.Priority = highPriority;
                    vCamOutside.Priority = lowPriority;
                }
            }
        }
    }
}
