using UnityEngine;

namespace PTWO_PR
{
    public static class GameTags
    {
        /// <summary>
        /// This class will hold variables and methods related to Tags
        /// We choose a static class, because it doesn't need to be initialized
        /// In a static class all fields and methods need to be static
        /// </summary>
        public static string PLAYER = "Player";

        public static string INTERACTABLEOBJECT = "InteractableObject";

        public static string JUMPPLATTFORM = "JumpPlatform";

        public static string ACTIVESAVEDATA = "ActiveSaveData";
        
        public static string SAVESYSTEMMANAGER = "SaveSystemManager";

        public static string MAINGAMEUI = "MainGameUI";

        public static string LEVELGATE = "LevelGate";

        public static string DAYNIGHTCYCLEMANAGER = "DayNightCycleManager";

        public static string SETTINGS = "Settings";

        public static string COLLECTIBLE = "Collectible";

        public static string COLLECTIBLEMANAGER = "CollectibleManager";

        public static string INVISIBLEINGAME = "InvisibleInGame";
        
        public static string COLLECTIBLEELEMENT = "CollectibleElement";

        public static string SPAWNPOINT = "SpawnPoint";

        /// <summary>
        /// A static method to find a GameObject by tag
        /// this method seeks only for Gameobjects with that specific Player-tag
        /// </summary>
        public static GameObject FindPlayer()
        {
            return GameObject.FindWithTag(PLAYER);
        }

        public static GameObject FindInteractableObject()
        {
            return GameObject.FindWithTag(INTERACTABLEOBJECT);
        }

        public static GameObject FindJumpPlatform()
        {
            return GameObject.FindWithTag(JUMPPLATTFORM);
        }

       

    }

}