using System;
using System.IO;
using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// The Save System Manager handles everything related to saving and loading
    /// </summary>
    public class SaveSystemManager : MonoBehaviour
    {
        [Header("There should only be one of this script in all times!")] 
        [SerializeField] private string fileEnding = ".gamesave"; //the fileEnding used for the saveFiles
        
        [Header("Save Data")] 
        [SerializeField] private PlayerData playerData;
        [SerializeField] private string currentSaveName;
        [SerializeField] private int currentSaveVersion;

        [Header("Autosave and Quicksave")] 
        [SerializeField] private string autosaveSuffix;
        [SerializeField] private string quicksaveSuffix;

        private ActiveSaveData activeSaveData; //A Reference to the active Save Data
        private string[] filePaths; //The paths to all gameSaves as array
        private string saveFilePath; //The path where the saveFiles are stored

        #region Properties
        public int CurrentSaveVersion
        { get => currentSaveVersion; set => currentSaveVersion = value; }
        
        public string CurrentSaveName
        { get => currentSaveName; set => currentSaveName = value; }
        
        public PlayerData PlayerData
        { get => playerData; set => playerData = value; }

        public string AutosaveSuffix
        { get => autosaveSuffix; set => autosaveSuffix = value; }

        public string QuicksaveSuffix
        { get => quicksaveSuffix; set => quicksaveSuffix = value; }
        #endregion
        
        #region Unity Functions
        private void Awake()
        {
            if (GameObject.FindGameObjectWithTag(GameTags.SAVESYSTEMMANAGER) != this.gameObject) 
                //have to make sure there is only one script to avoid wrong values/bugs
            {
                Destroy(this.gameObject);
            }
            
            DontDestroyOnLoad(this.gameObject); //this GO stays active throughout the whole game
            saveFilePath = Application.persistentDataPath;
        }

        private void Start()
        {
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
        }
        #endregion

        #region Public Functions
        //Use this functions to transform saves and do stuff in other classes:
        public void SaveGame(string saveName)
        {
            SaveFileToJson(saveName); //Saves the save file to the disk
        }

        public void SaveAutoSave(string saveName)
        {
            SaveFileToJson(saveName + autosaveSuffix);
        }

        public void SaveQuickSave(string saveName)
        {
            SaveFileToJson(saveName + quicksaveSuffix);
        }

        public string[] GetFilePaths()
        {
            return GetAllSaveFilePaths(); //Gets the paths to all save files as array
        }

        public PlayerData LoadGame(string saveName)
        {
            return LoadFromJson(saveName); //Loads the save file from the disk
        }

        public void PreloadSaveFile(string saveName)
        {
            LoadSaveFileToGameState(saveName); //Preloads a selected save-file to make it ready for activation
        }

        public void StartSaveFile()
        {
            ActivateLoadedSaveFile(); //Starts/Activates a save file by loading it into ActiveSaveFile
        }

        public void DeleteSave(string saveName)
        {
            DeleteSaveFile(saveName); //Deletes a save file from the disk
        }

        public bool DoesSaveExist(string saveName)
        {
            return DoesSaveNameExist(saveName); //Checks if a specific save file exists on the disk
        }

        public GameSaveTypes GetSaveType(string saveName)
        {
            if (IsAutosave(saveName))
            {
                return GameSaveTypes.AUTOSAVE;
            }else if (IsQuicksave(saveName))
            {
                return GameSaveTypes.QUICKSAVE;
            }
            return GameSaveTypes.DEFAULT;
        }

        public bool HasAutosave(string saveName)
        {
            return File.Exists(saveFilePath + "/" + saveName + autosaveSuffix + fileEnding);
        }
        
        public bool HasQuicksave(string saveName)
        {
            return File.Exists(saveFilePath + "/" + saveName + quicksaveSuffix + fileEnding);
        }
        #endregion
        
        
        
        #region Manager Functions

        private bool IsAutosave(string saveName)
        {
            return saveName.EndsWith(autosaveSuffix);
        }
        private bool IsQuicksave(string saveName)
        {
            return saveName.EndsWith(quicksaveSuffix);
        }
        
        //Saves the current playerData to the file with the given saveName
        private void SaveFileToJson(string saveName)
        {
            playerData = activeSaveData.GetData();
            playerData.saveVersion = currentSaveVersion;
            playerData.mainSaveName = saveName;
            playerData.dateTimeOfSave = DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss");
            string playerDataToSave = JsonUtility.ToJson(playerData);
            File.WriteAllText(saveFilePath + "/" + saveName + fileEnding, playerDataToSave);
            CurrentSaveName = saveName;
        }
        
        // Get all File Paths of all Saves and save them into array
        private string[] GetAllSaveFilePaths()
        {
            filePaths = Directory.GetFiles(saveFilePath,
                "*" + fileEnding); //Gets all paths to files that end with ".gamesave"(fileEnding)
            return filePaths;
        }

        // Load Gamesave from JSON-File with the given name
        private PlayerData LoadFromJson(string saveName)
        {
            PlayerData playerDataOut = new PlayerData();
            if (File.Exists(saveFilePath + "/" + saveName + fileEnding))
            {
                string loadedPlayerData = File.ReadAllText(saveFilePath + "/" + saveName + fileEnding);
                playerDataOut = JsonUtility.FromJson<PlayerData>(loadedPlayerData);
            }
            else
            {
                Debug.Log("No PlayerData SaveFile for " + saveName + " found!");
            }

            return playerDataOut;
        }

        // Load specific save file and put data into active playerData
        private void LoadSaveFileToGameState(string saveName)
        {
            playerData = LoadFromJson(saveName);
            currentSaveName = saveName;
        }

        // Activates the currently loaded save file and puts it into the ActiveSaveData Class
        private void ActivateLoadedSaveFile()
        {
            activeSaveData.SetData(playerData);
        }

        private void DeleteSaveFile(string saveName)
        {
            string filePath = saveFilePath + "/" + saveName + fileEnding;
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        // Checks if a specific save file exists and returns the result as bool
        private bool DoesSaveNameExist(string saveName)
        {
            bool doesExist = false;

            GetAllSaveFilePaths();
            foreach (string save in filePaths)
            {
                string fileName = Path.GetFileNameWithoutExtension(save);
                if (fileName == saveName)
                {
                    doesExist = true;
                }
            }

            return doesExist;
        }

        #endregion
    }

    public enum GameSaveTypes
    {
        DEFAULT, AUTOSAVE, QUICKSAVE
    }

    public enum CollectedElementTypes
    {
        NONE, EARTH, WATER, FIRE, WIND, ALL
    }
}