using UnityEngine;

namespace PTWO_PR
{
    [DisallowMultipleComponent]
    public class ActiveSaveData : MonoBehaviour
    {
        [Header("There should only be one of this script in all times!")] [SerializeField]
        private PlayerData playerData;

        private void Start()
        {
            //Making sure there is only one class of ActiveSaveData available at a time:
            if (GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA) != this.gameObject)
            {
                Destroy(this.gameObject);
            }
            
            DontDestroyOnLoad(this);
        }

        /// <summary>
        /// Set the Data of the currently active Savefile
        /// </summary>
        /// <param name="playerDataIn"></param>
        public void SetData(PlayerData playerDataIn)
        {
            playerData = playerDataIn;
        }

        /// <summary>
        /// Get the data of the current active SaveData
        /// </summary>
        /// <returns>(PlayerData) active SaveData-Variables</returns>
        public PlayerData GetData()
        {
            if (playerData != null)
            {
                return playerData;
            }

            //If there is no SaveData stored yet (for example in a new Game) use a new PlayerData-Class
            playerData = new PlayerData();
            return playerData;
        }

        /// <summary>
        /// Sets the saved position of the player
        /// </summary>
        /// <param name="position"></param>
        public void SetPosition(Vector3 position)
        {
            playerData.playerPosition = position;
        }

        /// <summary>
        /// Gets the Position of the loaded savedata
        /// </summary>
        /// <returns></returns>
        public Vector3 GetPosition()
        {
            return playerData.playerPosition;
        }

        public void AddCollectable(int idIn)
        {
            playerData.collectedCollectables += 1;
            playerData.collectibleCollected[idIn] = true;
        }

        /// <summary>
        /// Add a collected Element to the save file
        /// </summary>
        /// <param name="element">(CollectedElementTypes) Type of element that got collected</param>
        public void AddElement(CollectedElementTypes element) //Adding weather or not the player has picked up certain elements
        {
            if (element == CollectedElementTypes.EARTH)
            {
                playerData.hasEarthElement = true;
            }
            else if (element == CollectedElementTypes.WATER)
            {
                playerData.hasWaterElement = true;
            }
            else if (element == CollectedElementTypes.FIRE)
            {
                playerData.hasFireElement = true;
            }
            else if (element == CollectedElementTypes.WIND)
            {
                playerData.hasWindElement = true;
            }
        }

        public void InitializeCollectables(int collectibleCountIn) //how many collectibles have been picked up
        {
            if (playerData.collectibleCollected.Length == 0)
            {
                playerData.collectibleCollected = new bool[collectibleCountIn];
                Debug.Log("Length of collectibles: " + collectibleCountIn);
            }
        }

        public void DoesDayNightCycle(bool doDayNightCycleIn)
        {
            playerData.doesDayNightCycle = doDayNightCycleIn;
        }
    }
}