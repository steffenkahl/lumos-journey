using System;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PTWO_PR
{
    //Script for handling all input and systems at the menu for savefiles
    public class SaveMenuManager : MonoBehaviour
    {
        [Header("References")] 
        [SerializeField] private GameObject saveListContent; //Reference to the holding saveListContent
        [SerializeField] private GameObject saveListEntry; //Reference to the prefab for savelistentries

        [Header("New Save Panel References")]
        [SerializeField] private TMP_InputField newSaveNameInputField;
        [SerializeField] private TMP_Text inputFieldPlaceholderText;
        [SerializeField] private GameObject newSaveButton;
        [SerializeField] private GameObject overrideSaveButton;
        [SerializeField] private Button playButton;
        [SerializeField] private Button deleteButton;

        [Header("Scene Names")] 
        [SerializeField] private string gameSceneName;
        [SerializeField] private string mainMenuSceneName;
        
        [Header("Fade Settings")]
        [SerializeField] private Image menuCover;
        [SerializeField] private float fadeOutDuration;
        
        private Button newSaveButtonBtn;
        private string lastLoadedSaveName;
        
        private SaveSystemManager saveSystemManager; //Reference to the SaveSystemManager
        private string[] filePaths; //The paths to all gameSaves as array
        

        #region Unity Functions
        private void Start()
        {
            newSaveButtonBtn = newSaveButton.GetComponent<Button>();
            saveSystemManager = GameObject.FindGameObjectWithTag(GameTags.SAVESYSTEMMANAGER).GetComponent<SaveSystemManager>();
            if (saveSystemManager.CurrentSaveName != "")
            {
                newSaveNameInputField.text = saveSystemManager.CurrentSaveName; //set the inputField-Text to current save name if not empty
            }

            ShowSaves();
        }

        private void Update()
        {
            if (lastLoadedSaveName != saveSystemManager.CurrentSaveName)
            {
                newSaveNameInputField.text = saveSystemManager.CurrentSaveName; //if the current active save name has changed, change the inputField to current save name
                lastLoadedSaveName = saveSystemManager.CurrentSaveName; //change the local current Text input to the new name from the manager to detect the change
            }

            if (saveSystemManager.CurrentSaveName != "" && saveSystemManager.CurrentSaveName != "")
            {
                if (saveSystemManager.DoesSaveExist(saveSystemManager.CurrentSaveName))
                {
                    deleteButton.interactable = true; //if filename is not empty and does exist, activate delete button
                }
            }
            else
            {
                deleteButton.interactable = false; //else disable button
            }

            if (newSaveNameInputField.text != "")
            { //if the current save file is not empty, activate play button
                newSaveButtonBtn.interactable = true;
                playButton.interactable = true;
            }
        }
        #endregion

        #region Manager Functions
        /// <summary>
        /// Show all Save Files in List of SaveFilePrefabs
        /// </summary>
        public void ShowSaves()
        {
            filePaths = saveSystemManager.GetFilePaths();

            //First destroy all save buttons
            if (saveListContent.transform.childCount > 0)
            {
                newSaveNameInputField.text = "";
                saveSystemManager.CurrentSaveName = "";
                List<GameObject> children = new List<GameObject>();
                foreach (Transform child in saveListContent.transform)
                {
                    children.Add(child.gameObject);
                }

                children.ForEach(Destroy);
            }

            //then create new ones based on the files
            foreach (String saveFile in filePaths)
            {
                if (saveSystemManager.GetSaveType(Path.GetFileNameWithoutExtension(saveFile)) == GameSaveTypes.DEFAULT)
                {
                    //Instantiate Save File Button:
                    GameObject entry = Instantiate(saveListEntry, saveListContent.transform, true); //Create an entry for the saves List
                    SaveListEntryData saveListEntryData = entry.GetComponent<SaveListEntryData>();

                    entry.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    Vector3 position = entry.transform.position;
                    entry.transform.localPosition = new Vector3(position.x, position.y, 0f);

                    saveListEntryData.InitialValues(Path.GetFileNameWithoutExtension(saveFile), saveSystemManager);
                }
            }
        }
        #endregion
        
        #region ButtonEvents
        public void OnNewButtonPress()
        {
            //Create a new Save with the given Savename
            if (newSaveNameInputField.text == "")
            {
                inputFieldPlaceholderText.text = "ENTER NAME PLEASE";
            }
            else
            {
                saveSystemManager.SaveGame(newSaveNameInputField.text);
                ShowSaves();
                newSaveNameInputField.text = "";
            }
        }

        public void OnOverrideButtonPress()
        {
            //Override the selected Savefile
            saveSystemManager.SaveGame(newSaveNameInputField.text);
            ShowSaves();
        }

        public void OnInputFieldChange()
        {
            if (saveSystemManager.DoesSaveExist(newSaveNameInputField.text))
            {
                newSaveButton.SetActive(false);
                overrideSaveButton.SetActive(true);
            }
            else
            {
                newSaveButton.SetActive(true);
                overrideSaveButton.SetActive(false);
            }

            if (newSaveNameInputField.text.EndsWith(saveSystemManager.AutosaveSuffix))
            {
                newSaveButton.SetActive(false);
                overrideSaveButton.SetActive(false);
            }
            if (newSaveNameInputField.text.EndsWith(saveSystemManager.QuicksaveSuffix))
            {
                newSaveButton.SetActive(false);
                overrideSaveButton.SetActive(false);
            }
        }

        public void OnPlayButtonPress()
        {
            //Start a save file if you press play in the menu
            saveSystemManager.StartSaveFile();
            string currentName = saveSystemManager.CurrentSaveName;
            if (currentName.EndsWith(saveSystemManager.AutosaveSuffix))
            {
                int amountOfLetters = saveSystemManager.AutosaveSuffix.Length;
                saveSystemManager.CurrentSaveName = currentName.Remove(currentName.Length - amountOfLetters, amountOfLetters);
            }
            if (currentName.EndsWith(saveSystemManager.QuicksaveSuffix))
            {
                int amountOfLetters = saveSystemManager.QuicksaveSuffix.Length;
                saveSystemManager.CurrentSaveName = currentName.Remove(currentName.Length - amountOfLetters, amountOfLetters);
            }
            menuCover.DOFade(1, fadeOutDuration).OnComplete(OnPlayButtonLogic);
        }

        private void OnPlayButtonLogic()
        {
            SceneManager.LoadScene(gameSceneName);
        }

        public void OnBackToMenuButtonPress()
        {
            //Go back to the main menu
            SceneManager.LoadScene(mainMenuSceneName);
        }

        public void OnDeleteButtonPress()
        {
            //Delete the selected save and set the currentSaveName to null to prevent false loading
            saveSystemManager.DeleteSave(saveSystemManager.CurrentSaveName);
            saveSystemManager.CurrentSaveName = "";
            
            //Also reload the current saves
            ShowSaves();
        }
        #endregion
    }
}