using System;
using UnityEngine;

namespace PTWO_PR
{
    [Serializable]
    public class PlayerData
    {
        //All information that need to be saved
        public int saveVersion;
        public string mainSaveName;
        public Vector3 playerPosition;
        public string dateTimeOfSave;
        public bool hasEarthElement;
        public bool hasWaterElement;
        public bool hasFireElement;
        public bool hasWindElement;
        public bool[] collectibleCollected;
        public int collectedCollectables;
        public bool doesDayNightCycle;
    }
}