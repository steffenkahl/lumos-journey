using System;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PTWO_PR
{
    /// <summary>
    /// Sets the save name for the game when starting a new game
    /// </summary>
    public class SaveNameSetter : MonoBehaviour
    {
        [SerializeField] private string gameSceneName;
        [SerializeField] private string mainMenuSceneName;
        [SerializeField] private Image menuCover;
        [SerializeField] private float fadeOutDuration;
        [SerializeField] private Button setSaveNameButton;
        [SerializeField] private GameObject noSpecialCharactersInfo;
        private string inputtedSaveName;
        private SaveSystemManager saveSystemManager;

        private void Start()
        {
            setSaveNameButton.interactable = false;
            noSpecialCharactersInfo.SetActive(false);
            saveSystemManager = GameObject.FindGameObjectWithTag(GameTags.SAVESYSTEMMANAGER).GetComponent<SaveSystemManager>();
        }

        public void ChangeSaveName(string inputText)
        {
            noSpecialCharactersInfo.SetActive(false);
            if (!String.IsNullOrEmpty(inputText))
            {
                if (inputText.All(c => char.IsLetterOrDigit(c)))
                {
                    inputtedSaveName = inputText;
                    setSaveNameButton.interactable = true;
                }
                else
                {
                    setSaveNameButton.interactable = false;
                    noSpecialCharactersInfo.SetActive(true);
                }
            }
            else
            {
                setSaveNameButton.interactable = false;
            }
        }

        public void SetSaveName()
        {
            menuCover.DOFade(1, fadeOutDuration).OnComplete(SetSaveNameLogic);
        }

        private void SetSaveNameLogic()
        {
            saveSystemManager.CurrentSaveName = inputtedSaveName;
            saveSystemManager.SaveGame(inputtedSaveName);
            saveSystemManager.LoadGame(inputtedSaveName);
            saveSystemManager.StartSaveFile();
            SceneManager.LoadScene(gameSceneName);
        }

        public void BackToMenu()
        {
            SceneManager.LoadScene(mainMenuSceneName);
        }
    }
}