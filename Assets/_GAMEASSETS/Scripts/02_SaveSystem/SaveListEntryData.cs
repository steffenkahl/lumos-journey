using System;
using System.Globalization;
using TMPro;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    //All logic inside a saveFileEntry in the saveMenu
    public class SaveListEntryData : MonoBehaviour
    {
        [SerializeField] private string saveName = "-undefined-"; //the player can set the save name themselves
        [SerializeField] private int currentSaveVersion; //which is the selected one

        [Space] [Header("Colors")]
        [SerializeField] private Color oldVersionColor;
        [SerializeField] private Color newerVersionColor;
        [SerializeField] private Color selectedColor;
        [SerializeField] private Color standardColor;
        
        [Space] [Header("References")]
        [SerializeField] private Image mainButtonImage;
        [SerializeField] private Button mainsaveButton;
        [SerializeField] private Button autosaveButton;
        [SerializeField] private Button quicksaveButton;
        [SerializeField] private TMP_Text saveFileNameTxt;
        [SerializeField] private TMP_Text collectibleCountTxt;
        [SerializeField] private TMP_Text dateTxt;
        [SerializeField] private GameObject earthElementIcon;
        [SerializeField] private GameObject waterElementIcon;
        [SerializeField] private GameObject fireElementIcon;
        [SerializeField] private GameObject windElementIcon;
        
        [Header("Connected PlayerData")]
        [SerializeField] [ReadOnly] private PlayerData connectedPlayerData;
        
        private int collectibleCount;
        private bool hasEarthElement;
        private bool hasWaterElement;
        private bool hasFireElement;
        private bool hasWindElement;
        
        private int saveVersion = 1;
        private DateTime saveDate = DateTime.Now;
        private SaveSystemManager saveSystemManager;

        private bool hasAutosave;
        private bool hasQuicksave;
        private string currentDisplayedSaveName;
        private Image mainsaveButtonImage;
        private Image autosaveButtonImage;
        private Image quicksaveButtonImage;

        private GameSaveTypes selectedSaveType;

        private void Awake()
        {
            //Get needed references
            mainsaveButtonImage = mainsaveButton.gameObject.GetComponent<Image>();
            autosaveButtonImage = autosaveButton.gameObject.GetComponent<Image>();
            quicksaveButtonImage = quicksaveButton.gameObject.GetComponent<Image>();
        }

        private void Start()
        {
            //Set standard values
            mainButtonImage.color = standardColor;
            currentSaveVersion = saveSystemManager.CurrentSaveVersion;

            //Check saveVersion
            if (saveVersion < currentSaveVersion)
            {
                mainButtonImage.color = oldVersionColor;
            }
            else if (saveVersion > currentSaveVersion)
            {
                mainButtonImage.color = newerVersionColor;
            }

            //Check Save Types
            if (!hasAutosave)
            {
                autosaveButton.interactable = false;
            }
            if (!hasQuicksave)
            {
                quicksaveButton.interactable = false;
            }
        }

        private void Update()
        {
            if (saveSystemManager.CurrentSaveName == currentDisplayedSaveName)
            {
                mainButtonImage.color = selectedColor;
            }
            else
            {
                mainButtonImage.color = standardColor;
                mainsaveButtonImage.color = standardColor;
                autosaveButtonImage.color = standardColor;
                quicksaveButtonImage.color = standardColor;
            }
        }

        private void DisplayData()
        {
            //Set the displayed values
            saveFileNameTxt.text = currentDisplayedSaveName;
            collectibleCountTxt.text = collectibleCount.ToString();
            dateTxt.text = saveDate.ToString("dd.MM.yyyy hh:mm:ss", CultureInfo.CurrentCulture);
            
            mainsaveButtonImage.color = standardColor;
            autosaveButtonImage.color = standardColor;
            quicksaveButtonImage.color = standardColor;
            
            if (selectedSaveType == GameSaveTypes.DEFAULT)
            {
                mainsaveButtonImage.color = selectedColor;
            } else if (selectedSaveType == GameSaveTypes.AUTOSAVE)
            {
                autosaveButtonImage.color = selectedColor;
            } else if (selectedSaveType == GameSaveTypes.QUICKSAVE)
            {
                quicksaveButtonImage.color = selectedColor;
            }

            earthElementIcon.SetActive(false);
            waterElementIcon.SetActive(false);
            fireElementIcon.SetActive(false);
            windElementIcon.SetActive(false);
            if (hasEarthElement) earthElementIcon.SetActive(true);
            if (hasWaterElement) waterElementIcon.SetActive(true);
            if (hasFireElement) fireElementIcon.SetActive(true);
            if (hasWindElement) windElementIcon.SetActive(true);
        }

        /// <summary>
        /// Set Values for the SaveListEntry to show in the list
        /// </summary>
        /// <param name="saveNameIn">(string) name of the saveFile</param>
        /// <param name="saveSystemManagerIn">(SaveSystemManager) to get a reference to the correct SaveSystemManager without GameObject.Find()</param>
        public void SetValues(string saveNameIn)
        {
            connectedPlayerData = saveSystemManager.LoadGame(saveNameIn);
            
            collectibleCount = connectedPlayerData.collectedCollectables;
            hasEarthElement = connectedPlayerData.hasEarthElement;
            hasWaterElement = connectedPlayerData.hasWaterElement;
            hasFireElement = connectedPlayerData.hasFireElement;
            hasWindElement = connectedPlayerData.hasWindElement;
            saveVersion = connectedPlayerData.saveVersion;
            saveDate = DateTime.ParseExact(connectedPlayerData.dateTimeOfSave, "dd.MM.yyyy hh:mm:ss", null);
            DisplayData();
        }

        public void InitialValues(string saveNameIn, SaveSystemManager saveSystemManagerIn)
        {
            saveName = saveNameIn;
            saveSystemManager = saveSystemManagerIn;
            hasAutosave = saveSystemManager.HasAutosave(saveName);
            hasQuicksave = saveSystemManager.HasQuicksave(saveName);
            
            SetValues(saveNameIn);
            currentDisplayedSaveName = saveNameIn;
            selectedSaveType = GameSaveTypes.DEFAULT;
            DisplayData();
        }

        public void OnSaveFileClick()
        {
            OnLoadMainsaveClick();
        }
        
        public void OnLoadMainsaveClick()
        {
            currentDisplayedSaveName = saveName;
            saveSystemManager.PreloadSaveFile(saveName);
            selectedSaveType = GameSaveTypes.DEFAULT;
            SetValues(currentDisplayedSaveName);
        }
        
        public void OnLoadAutosaveClick()
        {
            currentDisplayedSaveName = saveName + saveSystemManager.AutosaveSuffix;
            saveSystemManager.PreloadSaveFile(currentDisplayedSaveName);
            selectedSaveType = GameSaveTypes.AUTOSAVE;
            SetValues(currentDisplayedSaveName);
        }
        
        public void OnLoadQuicksaveClick()
        {
            currentDisplayedSaveName = saveName + saveSystemManager.QuicksaveSuffix;
            saveSystemManager.PreloadSaveFile(currentDisplayedSaveName);
            selectedSaveType = GameSaveTypes.QUICKSAVE;
            SetValues(currentDisplayedSaveName);
        }
    }
}