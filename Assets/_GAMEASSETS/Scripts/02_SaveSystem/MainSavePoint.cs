using DG.Tweening;
using TMPro;
using UnityEngine;

namespace PTWO_PR
{
    public class MainSavePoint : MonoBehaviour
    {
        [SerializeField] private bool isUsable;
        [SerializeField] private bool inRange; //you cannot save if the players distance is too far from the savepoint
        [SerializeField] private TMP_Text infoText; //giving the info that you can save with "E"
        [SerializeField] private GameObject infoTextHolder;
        private SaveSystemManager saveSystemManager;
        private ActiveSaveData activeSaveData;
        private CharacterControllerMovement player;
        private MainGameUI mainGameUI;
        private Camera mainCamera;

        private void Start()
        {
            //Finding and setting references
            player = GameObject.FindGameObjectWithTag(GameTags.PLAYER).GetComponent<CharacterControllerMovement>();
            saveSystemManager = GameObject.FindGameObjectWithTag(GameTags.SAVESYSTEMMANAGER).GetComponent<SaveSystemManager>();
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
            mainGameUI = GameObject.FindGameObjectWithTag(GameTags.MAINGAMEUI).GetComponent<MainGameUI>();
            mainCamera = Camera.main;
            HideInfo();
        }

        private void Update()
        {
            if (mainCamera == null)
            {
                Debug.Log("Camera missing!");
            }
            infoTextHolder.transform.LookAt(mainCamera.transform.position);
                //DOLookAt(mainCamera.transform.position, 0.5f);
        }

        private void OnTriggerEnter(Collider other) //If the player is in the collider (whiich is bigger than the model, whow the info that the palyer can save
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                ShowInfo();
            }
        }

        private void OnTriggerExit(Collider other) //if he leaves the collider, make the info disappear 
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                HideInfo();
            }
        }

        private void ShowInfo()
        {
            inRange = true;
            player.ActiveSavePoint = this;
            infoText.DOFade(1, 0.2f);
        }
        private void HideInfo()
        {
            inRange = false;
            player.ActiveSavePoint = null;
            infoText.DOFade(0, 0.2f);
        }

        public void SaveTheGame() //saving when pressing "E"
        {
            if (inRange)
            {
                mainGameUI.ShowSaveInfo(); //Show Saving Animation
                Debug.Log("Save current savename: " + saveSystemManager.CurrentSaveName);
                activeSaveData.SetPosition(player.transform.position + Vector3.up * 0.1f); //Set the player Position in the save upper to not fall through ground

                saveSystemManager.SaveGame(saveSystemManager.CurrentSaveName);
                mainGameUI.HideSaveInfo(2f); //Hide Saving Animation after saving
            }
        }
    }
}