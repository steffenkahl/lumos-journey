using DG.Tweening;
using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// Script that handles the day night cycle
    /// </summary>
    public class DayNightCycleManager : MonoBehaviour
    {
        [Range(0, 24)] public float timeOfDay = 12; //Has to be public so it can be used in the editor

        [SerializeField] private GameObject sun;
        [SerializeField] private float standardLuxOfTheSun;
        private Light sunLight;

        [SerializeField] private float secondsPerMinute = 60;
        private float secondsPerHour;
        private float secondsPerDay;

        [SerializeField] private float timeMultiplier = 1;
        [SerializeField] private bool doesDayNightCycle;
        [SerializeField] private float dayTimeChangeTriggerSpeed;
        private ActiveSaveData activeSaveData;

        #region Unity Methods
        
        private void Start()
        {
            //Calculate time variables
            secondsPerHour = secondsPerMinute * 60;
            secondsPerDay = secondsPerHour * 24;
            sunLight = sun.GetComponent<Light>();
            activeSaveData = GameObject.FindGameObjectWithTag(GameTags.ACTIVESAVEDATA).GetComponent<ActiveSaveData>();
            doesDayNightCycle = activeSaveData.GetData().doesDayNightCycle;
        }

        /// <summary>
        /// Allow changing the timeOfDay in the editor
        /// </summary>
        private void OnValidate()
        {
            sunLight = sun.GetComponent<Light>();
            SunUpdate();
            ChangeLighting();
        }

        private void Update()
        {
            SunUpdate(); //Rotate the sun
            ChangeLighting(); //Change the lighting intensity
            
            //Reset the time of day if midnight is hit
            if (timeOfDay >= 24)
            {
                timeOfDay = 0;
            }
            
            if (doesDayNightCycle)
            {
                //Calculate the time of day with the seconds per Day and the Time Multiplier to make time run faster/slower then real time
                timeOfDay += (Time.deltaTime / secondsPerDay) * timeMultiplier;
            }
        }

        /// <summary>
        /// Updates (rotates) the sun according to the current time
        /// </summary>
        private void SunUpdate()
        {
            //Rotate the sun around
            sun.transform.localRotation = Quaternion.Euler(((timeOfDay / 24) * 360f) - 90, 90, 0);
        }

        #endregion

        /// <summary>
        /// Changes the lighting according to the daytime
        /// </summary>
        private void ChangeLighting()
        {
            //Change the lighting intensity based on the time of day
            if (timeOfDay >= 22)
            {
                sunLight.intensity = standardLuxOfTheSun * 0.5f;
            } 
            else if(timeOfDay > 23.5 || timeOfDay < 2)
            {
                sunLight.intensity = 0f;
            }
            else if(timeOfDay >= 2 && timeOfDay < 6)
            {
                sunLight.intensity = standardLuxOfTheSun * 0.5f;
            }
            else
            {
                sunLight.intensity = standardLuxOfTheSun;
            }
        }

        /// <summary>
        /// Switches if the day night cycle is active
        /// </summary>
        /// <param name="doesDayNightCycleIn">(bool) decides if the daynightcycle should be active</param>
        public void DoDayNightCycle(bool doesDayNightCycleIn)
        {
            doesDayNightCycle = doesDayNightCycleIn;
        }

        /// <summary>
        /// Returns if the day night cycle is active at the moment
        /// </summary>
        /// <returns></returns>
        public bool DayNightCycleIsActive()
        {
            return doesDayNightCycle;
        }

        /// <summary>
        /// Change the time of day to a specific time
        /// </summary>
        /// <param name="dayTimeIn">(float) time to switch to</param>
        /// <param name="changeSpeed">(float) how fast the change should be</param>
        public void ChangeDayTime(float dayTimeIn, float changeSpeed)
        {
            if (dayTimeIn >= timeOfDay)
            {
                DOTween.To(() => timeOfDay, x => timeOfDay = x, dayTimeIn, changeSpeed);
            }
            else
            {
                Sequence mySequence = DOTween.Sequence();
                mySequence.Append(DOTween.To(() => timeOfDay, x => timeOfDay = x, 23.9f, changeSpeed / 2));
                mySequence.Append(DOTween.To(() => timeOfDay, x => timeOfDay = x, 0f, 0.01f));
                mySequence.Append(DOTween.To(() => timeOfDay, x => timeOfDay = x, dayTimeIn, changeSpeed / 2));
            }
        }
    }
}